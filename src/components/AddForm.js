import React, { useState } from 'react';
import PropTypes from 'prop-types';

const AddForm = (props) => {
    const [nameText, setNameText] = useState('');
    const [colorText, setColorText] = useState('');

    const handleSubmit = (evt) => {
        evt.preventDefault();
        props.onAdd(evt,nameText,colorText);
        // console.log('handleSubmit', props);
        // props.setAddOpen(false);
    }
    return (
        <div>
            <label>
                text:
                <input
                    type="text"
                    name="name"
                    value={nameText}
                    onChange={e => setNameText(e.target.value)}
                />
            </label>
            <label>
                color:
                <input
                    type="text"
                    name="color"
                    value={colorText}
                    onChange={e => setColorText(e.target.value)}
                />
            </label>
            <button onClick={handleSubmit}>ADD</button>
        </div>

    )
}

AddForm.propTypes = {
    onAdd: PropTypes.any
};

export default AddForm;