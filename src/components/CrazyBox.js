import React, { useState,useEffect } from 'react';
import PropTypes from 'prop-types';
import AddForm from './AddForm';
import '../style/CrazyBox.css';

const CrazyBox = ({ name, color, crazyBoxList }) => {

    const [addOpen, setAddOpen] = useState(false);
    const [localCrazyBoxList, setLocalCrazyBoxList] = useState([]);


    useEffect(()=>{
        // console.log('effect')
        setLocalCrazyBoxList(crazyBoxList);
    },[addOpen])

    const onAddHandler = (a,b,c) => {
        // console.log('onAddHandler',a,b,c);
        let newCrazyBoxList = [...crazyBoxList,{name:b,color:c,crazyBoxList:[]}];
        // console.log('crazyBoxList',crazyBoxList)
        // console.log('newCrazyBoxList',newCrazyBoxList)
        setLocalCrazyBoxList(newCrazyBoxList);
        setAddOpen(false);
    }

    return (
        <div
            className="crazy-box"
            style={{ backgroundColor: `${color}` }}
        >
            <h1>{name}</h1>
            <h3>{color}</h3>
            <button
                onClick={() => setAddOpen(!addOpen)}
            > + </button>
            {
                addOpen ?
                    <AddForm onAdd={onAddHandler} ></AddForm>
                    : null
            }
            <div>
                {
                    crazyBoxList &&
                    crazyBoxList.map((props, index) => (
                        <CrazyBox
                            key={index}
                            name={props.name}
                            color={props.color}
                            crazyBoxList={props.crazyBoxList}
                        ></CrazyBox>
                    ))
                }
            </div>
        </div>
    )
}

let crazyBoxProps = {
    name: PropTypes.string,
    color: PropTypes.string,
};

crazyBoxProps.crazyBoxList = PropTypes.arrayOf(
    PropTypes.shape(crazyBoxProps)
);

CrazyBox.propTypes = crazyBoxProps;

export default CrazyBox;
